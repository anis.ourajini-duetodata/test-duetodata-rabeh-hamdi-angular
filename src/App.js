
import './App.css';
import BasicExample from "./Posts/Posts"
import BasicExample2 from "./Posts/Posts2"
import customData from './data/TF1-2021-06-29.json';
function App() {
  return (
    <div className="App">
      <BasicExample customData={customData[0].post}/>
      <br/><br/><br/><br/><br/><br/><br/>
      <BasicExample2 customData={customData[0].post}/>
    </div>
  );
}

export default App;
