import customData from '../data/TF1-2021-06-29.json'; 
console.log("ccc == ",customData[0].posts)
function BasicExample() {
  return (
    <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Posts</th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
  { 
  customData[0].posts.map((el,index)=>
    <tr>
      <th scope="row">{index}</th>
      <td>{el.post_text}</td>
      <td>{el.post_comments.length } comments</td>
      <td>{el.post_date}</td>
    </tr>
  )
}
    
    
  </tbody>
</table>

 
  );
}

export default BasicExample;